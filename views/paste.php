<html>
    <body style='overflow: hidden; padding: 0; margin: 0;'>
<?php if (isset($name) && $name): ?>
        <form action='/<?= $name; ?>/f' method='POST'>
<?php else: ?>
        <form action='/' method='POST'>
<?php endif; ?>
            <textarea name='paste' style='position: absolute; top: 0%; left: 0%; width: 100%; height: 100%;' autofocus><?= $file; ?></textarea>
            <button type='submit' style='position: absolute; top: 10px; right: 9px;'>
<?php if (isset($name) && $name): ?>
                fork
<?php else: ?>
                paste
<?php endif; ?>
            </button>
<?php if ($isFork): ?>
            <h3 style='position: absolute; bottom: -13px; right: 12px;'>fork of <a href='/<?= $of; ?>'><?= $of; ?></a></h3>
<?php endif; ?>
        </form>
    </body>
</html>

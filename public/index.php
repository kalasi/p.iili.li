<?php

require __DIR__ . './../vendor/autoload.php';

define('FORKS', __DIR__ . './../storage/forks/');
define('UPLOAD', __DIR__ . './../storage/upload/');
define('VIEWS', __DIR__ . './../views/');

function generateName() {
    // Seed
    $seed = str_split('abcdefghijklmnopqrstuvwxyz'
        . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        . '0123456789!@$^*()[]{}|;:_');

    // Find a filename.
    do {
        $name = '';
        foreach (array_rand($seed, 4) as $k) $name .= $seed[$k];
    } while (file_exists(UPLOAD . $name));

    return $name;
}

$app = new \Slim\Slim();

$app->get('/', function () {
    $isFork = false;
    $name = '';
    $file = '';

    require VIEWS . 'paste.php';
});

$app->post('/', function () use ($app) {
    $paste = $app->request->post('paste');

    $name = generateName();

    file_put_contents(UPLOAD . $name, $paste);

    $app->response->redirect('/' . $name, 303);
});

// Paste.
$app->get(':paste+', function ($paste) use ($app) {
    $name = $paste[1];

    $of = '';

    $isFork = file_exists(FORKS . $name);

    if ($isFork) {
        $of = file_get_contents(FORKS . $name);
    }

    $file = file_get_contents(UPLOAD . $name);

    require VIEWS . 'paste.php';
});

// Fork a paste.
$app->post(':paste+/f', function ($paste) use ($app) {
    $of = $paste[1];
    $content = $app->request->post('paste');

    $name = generateName();

    file_put_contents(UPLOAD . $name, $content);
    file_put_contents(FORKS . $name, $of);

    $app->response->redirect('/' . $name, 303);
});

$app->run();
